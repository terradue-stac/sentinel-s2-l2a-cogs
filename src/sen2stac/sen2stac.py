import os
import sys
import click
import logging
import requests
import json
import shutil
from pystac import Catalog, STAC_IO, CatalogType, read_file
from .stac import fix_item

logging.basicConfig(stream=sys.stderr, 
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')


    
@click.command()
@click.option('--target', '-t', 'target_dir', help='target directory')
@click.argument('identifiers', nargs=-1, required=True)
def main(target_dir, identifiers):
    
    items = []
    
    if not os.path.exists(target_dir):
    
        os.mkdir(target_dir)

    for identifier in identifiers:
        
        try:
            items.append(fix_item(identifier))
        except KeyError:
            pass
        
    catalog = Catalog(id='catalog',
                  description='staged STAC catalog')

    catalog.add_items(items)

    catalog.normalize_and_save(root_href=target_dir,
                               catalog_type=CatalogType.SELF_CONTAINED)

    catalog.describe()
    
    # fix URLs
    base_url = 'https://terradue-rtd.gitlab.io/sentinel-s2-l2a-cogs'

    for item in catalog.get_items():

        with open(os.path.join(target_dir, item.get_self_href())) as json_file:
            data = json.load(json_file)

        for index, link in enumerate(data['links']):

            new_link = '{}{}'.format(base_url, link['href']).replace('..', '')

            data['links'][index]['href'] = new_link

        with open(os.path.join(target_dir, item.get_self_href()), 'w') as f:
            json.dump(data, f, indent=4) 
    
    
    # fix catalog urls to items
    
    with open(os.path.join(target_dir, 'catalog.json')) as json_file:
        data = json.load(json_file)
    
    for index, link in enumerate(data['links']):

        new_link = '{}{}'.format(base_url, link['href']).replace(os.getcwd(), '').replace('./', '/')
        if link['rel'] == 'item':

            print(link['href'])
            
            item = read_file(os.path.join(target_dir, link['href']))
            
            sub_path = '/'.join([str(item.properties[x]) for x in ['sentinel:utm_zone', 
                                                'sentinel:latitude_band', 
                                                'sentinel:grid_square']])

            print(sub_path)
            new_link = '{}/{}{}'.format(base_url, sub_path, link['href']).replace(os.getcwd(), '').replace('./', '/')

            try:
                os.makedirs(os.path.join(target_dir, sub_path))
            except FileExistsError:
                pass
            # move the item dir to the subpath
            shutil.move(os.path.join(target_dir, os.path.dirname(link['href'])), 
                        os.path.join(target_dir, sub_path))

            #shutil.rmtree(os.path.join(target_dir, os.path.dirname(link['href'])))
            
            data['links'][index]['href'] = new_link

        else:

            new_link = '{}{}'.format(base_url, link['href']).replace(os.getcwd(), '').replace('./', '/')
            data['links'][index]['href'] = new_link

    with open(os.path.join(target_dir, 'catalog.json'), 'w') as f:
        json.dump(data, f, indent=4)
    
    
if __name__ == "__main__":
    main()
