import os
import sys
import click
import logging
import requests
import json
import shutil
from pystac import Catalog, STAC_IO, CatalogType, read_file, Link, write_file, LinkType
from urllib.parse import urlparse
from .stac import fix_item

logging.basicConfig(stream=sys.stderr, 
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')

def fix_catalog_children(cat_path, rel='child'):
    
    print('cat_path is ', cat_path)
    base_url = 'https://terradue-rtd.gitlab.io/sentinel-s2-l2a-cogs'

    # if the catalog exits, read it, else create a new one
    try:
        
        cat = Catalog.from_file(os.path.join(cat_path, 'catalog.json'))
        
        # remove any item the catalog may have if not adding an item
        if rel=='child': 
            cat.remove_links(rel='item')
        
    except FileNotFoundError:
        
        cat = Catalog(id=os.path.basename(cat_path),
                      description=os.path.basename(cat_path)) 
        
        cat.set_self_href(os.path.join(base_url, 
                                       cat_path.replace('./', ''),
                                       'catalog.json'))
        
        # only add a parent when needed
        if cat_path not in ['./']:
            #print(cat_path.replace('.', '').split('/')[:-1])
            if cat_path.replace('.', '').split('/')[:-1] == ['']:

                if cat.get_parent() is None:
                    cat.add_link(Link(rel='parent', target='/'.join([base_url, 
                                                                 'catalog.json'])))

            else:
                if cat.get_parent() is None:
                    cat.add_link(Link(rel='parent', target='/'.join([base_url, 
                                                                 *cat_path.replace('./', '').split('/')[:-1], 
                                                                 'catalog.json'])))
    
    rel_links = cat.get_links(rel=rel)

    if len(rel_links) == 0:
    
        for f in ['{}/{}/catalog.json'.format(base_url, f.path.replace('./', '')) for f in os.scandir(cat_path) if f.is_dir()]:
            
            if '.ipynb_checkpoints' in f: 
                continue
            
            if rel=='child':
                target = f
            elif rel=='item':
                item_name = os.path.dirname(urlparse(f)[2]).split('/')[-1]
                target = f.replace('catalog.json', '{0}.json'.format(item_name))
                #print('target ', target)
            else:
                continue
            print('adding rel {} {}'.format(rel, target))   
            rel_link = Link(rel=rel, target=target)
            cat.add_link(rel_link)
            
    else:
        
        if rel=='child':
            
            rel_on_fs = ['{}/{}/catalog.json'.format(base_url, 
                                                     f.path.replace('./', '')) for f in os.scandir(cat_path) if f.is_dir()]

            rel_in_stac = [link.get_href() for link in rel_links]
        
        if rel=='item':
            
            print([f.path for f in os.scandir(cat_path) if f.is_dir()])
            
            rel_on_fs = ['{}/{}/{}.json'.format(base_url,
                                                f.path.replace('./', ''),
                                                os.path.basename(urlparse(f.path)[2]).split('/')[-1]) for f in os.scandir(cat_path) if f.is_dir()]
            
            rel_in_stac = [link.get_href() for link in rel_links]
        
        for missing_element in list((set(rel_on_fs).difference(rel_in_stac))):
            
            rel_link = Link(rel=rel, target=missing_element)
            cat.add_link(rel_link)

                
    # write catalog
    dest_href=os.path.join(cat_path, 'catalog.json')
    write_file(cat, dest_href=dest_href)
    
def get_item_subpath(item):
    
    sub_path = '/'.join([str(item.properties[x]) for x in ['sentinel:utm_zone', 
                                                               'sentinel:latitude_band', 
                                                               'sentinel:grid_square']])
    return sub_path 

@click.command()
@click.option('--target', '-t', 'target_dir', help='target directory')
@click.argument('identifiers', nargs=-1, required=True)
def main(target_dir, identifiers):
    
    items = []
    is_new = False
    
    if not os.path.exists(target_dir):
    
        os.mkdir(target_dir)
    
    # check if there are new items to add
    for identifier in identifiers:
        
        try:

            item = fix_item(identifier)
            
        except KeyError:
            # add logging info about exception
            pass

        
        sub_path = get_item_subpath(item)
            
        # check if the item folder tree is already created
        if not os.path.exists(os.path.join(target_dir, sub_path, item.id)):
            
            print('Adding item {}'.format(item.id))
            
            try:
                os.makedirs(os.path.join(target_dir, 
                                         sub_path))
                
            except FileExistsError:
                pass
             
            write_file(item, 
                       dest_href=os.path.join(target_dir, 
                                              sub_path, 
                                              item.id, 
                                              '{}.json'.format(item.id)))
    
    os.chdir(target_dir) 
    # walk through the tree and fix the catalog.json along the way
    for x in os.walk('./'):

        if len(x[0].split('/')) < 4:
            
            # got a child 
            fix_catalog_children(x[0])
        
        elif len(x[0].split('/'))==4: 
            
            # got to an item
            fix_catalog_children(x[0], rel='item')

if __name__ == "__main__":
    main()
