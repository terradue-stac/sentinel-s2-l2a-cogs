from pystac import *
import os
from urllib.parse import urlparse
import requests
from pystac import STAC_IO

def identifier_to_item(identifier):
    
    item_id = '_'.join([identifier.split('_')[0],
                        identifier.split('_')[5][1:6],
                        identifier.split('_')[6][0:8],
                        '0',
                        'L2A'])
    
    return item_id

def my_read_method(uri):
    
    parsed = urlparse(uri)
    
    if parsed.scheme.startswith('http'):

        return requests.get(uri).text
            

    else:
        return STAC_IO.default_read_text_method(uri)
    
def get_item(identifier):
   
    item_id = identifier_to_item(identifier)

    url_template = f'https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/{item_id}'
    
    return url_template


bands = {'B01': {
                            "name": "B01",
                            "common_name": "coastal",
                            "center_wavelength": 0.4439,
                            "full_width_half_max": 0.027
                        },
                      'B02':  {
                            "name": "B02",
                            "common_name": "blue",
                            "center_wavelength": 0.4966,
                            "full_width_half_max": 0.098
                        },
                      'B03':  {
                            "name": "B03",
                            "common_name": "green",
                            "center_wavelength": 0.56,
                            "full_width_half_max": 0.045
                        },
                      'B04':  {
                            "name": "B04",
                            "common_name": "red",
                            "center_wavelength": 0.6645,
                            "full_width_half_max": 0.038
                        },
                       'B05': {
                            "name": "B05",
                            "center_wavelength": 0.7039,
                            "full_width_half_max": 0.019
                        },
                      'B06':  {
                            "name": "B06",
                            "center_wavelength": 0.7402,
                            "full_width_half_max": 0.018
                        },
                      'B07': {
                            "name": "B07",
                            "center_wavelength": 0.7825,
                            "full_width_half_max": 0.028
                        },
                       'B08': {
                            "name": "B08",
                            "common_name": "nir",
                            "center_wavelength": 0.8351,
                            "full_width_half_max": 0.145
                        },
                      'B8A':  {
                            "name": "B8A",
                            "center_wavelength": 0.8648,
                            "full_width_half_max": 0.033
                        },
                      'B09':  {
                            "name": "B09",
                            "center_wavelength": 0.945,
                            "full_width_half_max": 0.026
                        },
                      'B11':  {
                            "name": "B11",
                            "common_name": "swir16",
                            "center_wavelength": 1.6137,
                            "full_width_half_max": 0.143
                        },
                       'B12': {
                            "name": "B12",
                            "common_name": "swir22",
                            "center_wavelength": 2.22024,
                            "full_width_half_max": 0.242
                        }, 
                      'AOT':  {
                            "name": "AOT",
                            "center_wavelength": 0,
                            "full_width_half_max": 0
                        },
                       'SCL': {
                            "name": "SCL",
                            "center_wavelength": 0,
                            "full_width_half_max": 0
                        },
                     'WVP': {
                            "name": "WVP",
                            "center_wavelength": 0,
                            "full_width_half_max": 0
                        },
                     }
        
def fix_item(identifier):
    
    print(get_item(identifier))
    item = read_file(get_item(identifier))
    
    item.id = identifier
    
    eo_item = extensions.eo.EOItemExt(item)
    
    _bands = []

    for index, band in enumerate(bands.keys()):

        asset = item.get_assets()[band] 

        if 'common_name' in bands[band].keys():

            stac_band = extensions.eo.Band.create(name=band, 
                                                          common_name=bands[band]['common_name'],
                                                          description=bands[band]['name'])
        else:

            stac_band = extensions.eo.Band.create(name=band, 
                                                          description=bands[band]['name'])

        _bands.append(stac_band)

        eo_item.set_bands([stac_band], asset=asset)

    eo_item.set_bands(_bands)

    eo_item.apply(_bands)
    
    item.set_collection(None)
    
    item.clear_links()
    
    return item
