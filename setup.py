from setuptools import setup, find_packages
from io import open
import os

print(find_packages(where='src'))

console_scripts = """
[console_scripts]
sen2stac=sen2stac.sen2stac2:main
"""

setup(entry_points=console_scripts,
      packages=find_packages(where='src'),
      package_dir={'': 'src'})


